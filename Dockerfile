ARG DEBIANCODE=bullseye
FROM registry.opencode.de/ig-bvc/standard-images/grund-images/debian:$DEBIANCODE

CMD ["/bin/sh"]
